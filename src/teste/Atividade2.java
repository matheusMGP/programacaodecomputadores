package teste;

import java.util.Scanner;

/**
 * @category Atividade
 * @author Pedro Lucas - 20191119387
 * @author Matheus Gustavo - 20192113862
 * @author Rafael de Oliveira Machado - 20192113816
 */
public class Atividade2 {
    private static Scanner s = new Scanner(System.in);
    private static int[] fibonnacis;

    public static void main(String[] args) {
        questao01();
        questao04();
        questao05();
        questao06();
        questao07();
        questao08();
        questao09();
        questao10();
    }

    /**
     * Escreva um programa que imprima todos os nÃºmeros inteiros pares entre 0 e 36.
     */
    public static void questao01() {
        separar(25);
        escreverQuestao(1);

        for (int i = 0; i <= 36; i++) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }

        System.out.println();
    }

    /**
     * Escreva um programa que imprima os 20 primeiros nÃºmeros de Fibonacci.
     */
    public static void questao04() {
        separar(25);
        escreverQuestao(4);

        int numerosFib = 20;
        fibonnacis = new int[numerosFib];

        for (int i = 0; i < numerosFib; i++) {
            System.out.print(fibonnaci(i) + " ");

            // fibonnacis[i] = fibonnaciComMemorizacao(i);
            // System.out.print(fibonnacis[i] + " ");
        }

        System.out.println();
    }

    /**
     * Algoritmo de fibonnaci utilizando recursividade.
     * 
     * @param x o indice do fibonnaci
     * @return o valor do fibonnaci no indice @param x
     */
    public static int fibonnaci(int x) {
        return x <= 1 ? x : fibonnaci(x - 2) + fibonnaci(x - 1);
    }

    /**
     * Algoritmo de fibonnaci utilizando recursividade e memorizaÃ§Ã£o.
     * 
     * @param x o indice do fibonnaci
     * @return o valor do fibonnaci no indice @param x
     */
    public static int fibonnaciComMemorizacao(int x) {
        if (x <= 1) {
            fibonnacis[x] = x;
            return x;
        }

        return (fibonnacis[x - 2] == 0 ? fibonnaci(x - 2) : fibonnacis[x - 2])
                + (fibonnacis[x - 1] == 0 ? fibonnaci(x - 1) : fibonnacis[x - 1]);
    }

    /**
     * Ler duas idades e escrever qual delas Ã© a maior.
     */
    public static void questao05() {
        separar(25);
        escreverQuestao(5);
        System.out.print("Digite a 1Âº idade: ");
        int idade1 = s.nextInt();

        System.out.print("Digite a 2Âº idade: ");
        int idade2 = s.nextInt();

        System.out.println("A maior idade Ã©: " + Math.max(idade1, idade2));
    }

    /**
     * Entrar com um valor inteiro e dizer a qual mÃªs do ano pertence.
     */
    public static void questao06() {
        separar(25);
        escreverQuestao(6);
        System.out.print("Digite um nÃºmero de 1 a 12: ");
        int mes = s.nextInt();

        System.out.println("O mÃªs escolhido Ã©: " + obterMes(mes));
    }

    /**
     * 
     * @param mes indice correspondete ao mes do calendario
     * @return o mes do calendario referente ao indice @param mes
     */
    public static String obterMes(int mes) {
        switch (mes) {
            case 1:
                return "JANEIRO";
            case 2:
                return "FEVEREIRO";
            case 3:
                return "MARÃ‡O";
            case 4:
                return "ABRIL";
            case 5:
                return "MAIO";
            case 6:
                return "JUNHO";
            case 7:
                return "JULHO";
            case 8:
                return "AGOSTO";
            case 9:
                return "SETEMBRO";
            case 10:
                return "OUTUBRO";
            case 11:
                return "NOVEMBRO";
            case 12:
                return "DEZEMBRO";
            default:
                return "INVÃ�LIDO";
        }
    }

    /**
     * Ler duas notas fazer a mÃ©dia destas e escrever se o aluno foi aprovado ou
     * reprovado (mÃ©dia de aprovaÃ§Ã£o igual ou maior que 7).
     */
    public static void questao07() {
        separar(25);
        escreverQuestao(7);
        System.out.print("Digite a 1Âº nota: ");
        double n1 = s.nextDouble();

        System.out.print("Digite a 2Âº nota: ");
        double n2 = s.nextDouble();

        double media = (n1 + n2) / 2;

        System.out.printf("Sua mÃ©dia Ã© %.2f vocÃª esta %s\n", media, (media >= 7 ? "Aprovado" : "Reprovado"));
    }

    /**
     * Ler 3 lados do triÃ¢ngulo e dizer se este existe e se ele Ã© escaleno,
     * equilÃ¡tero ou isÃ³sceles.
     */
    public static void questao08() {
        separar(25);
        escreverQuestao(8);
        System.out.print("Digite o 1Âº lado do triÃ¢ngulo: ");
        double a = s.nextDouble();

        System.out.print("Digite o 2Âº lado do triÃ¢ngulo: ");
        double b = s.nextDouble();

        System.out.print("Digite o 3Âº lado do triÃ¢ngulo: ");
        double c = s.nextDouble();

        System.out.println(TipoTriangulo(a, b, c));
    }

    /**
     * 
     * @param a lado a do triÃ¢ngulo
     * @param b lado b do triÃ¢ngulo
     * @param c lado c do triÃ¢ngulo
     * @return tipo de triÃ¢ngulo de acordo com os valores dos lados fornecidos
     */
    public static String TipoTriangulo(double a, double b, double c) {
        if (a != b && a != c && b != c) {
            return "TriÃ¢ngulo Escaleno";
        } else if (a == b && a == c) {
            return "TriÃ¢ngulo EquilÃ¡tero";
        } else {
            return "TriÃ¢ngulo IsÃ³celes";
        }
    }

    /**
     * Leia um ponto qualquer com as coordenadas (x,y) de um plano cartesiano e
     * dizer em qual quadrante ele estÃ¡ situado, se estÃ¡ em um dos eixos ou
     * localizado na origem.
     */
    public static void questao09() {
        separar(25);
        escreverQuestao(9);
        System.out.print("Digite a coordenada x: ");
        double x = s.nextDouble();

        System.out.print("Digite a coordenada y: ");
        double y = s.nextDouble();

        String quadrante = indentificarQuadrante(x, y);

        System.out.println("O ponte x e y (" + x + ", " + y + ") estÃ¡ localizado no(a): " + quadrante);
    }

    /**
     * 
     * @param x coordenada x
     * @param y coordenada y
     * @return o quadrante que estÃ¡ localizado o ponto na coordenada x e y
     */
    public static String indentificarQuadrante(double x, double y) {
        if (x == 0 && y == 0)
            return "Origem";

        if (x == 0 && y != 0)
            return "Eixo Y";

        if (y == 0 && x != 0)
            return "Eixo X";

        if (x > 0) {
            if (y > 0)
                return "1Âº Quadrante";
            return "4Âº Quadrante";
        } else {
            if (y > 0)
                return "2Âº Quadrante";
            return "3Âº Quadrante";
        }
    }

    /**
     * Construa um programa para calcular as raÃ­zes de uma equaÃ§Ã£o do 2o grau a
     * partir dos seus coeficientes A, B e C. Lembre-se da possibilidade de raÃ­zes
     * iguais e de nÃ£o haves raÃ­zes real.
     */
    public static void questao10() {
        separar(25);
        escreverQuestao(10);
        System.out.print("Digite o valor de a: ");
        double a = s.nextDouble();

        System.out.print("Digite o valor de b: ");
        double b = s.nextDouble();

        System.out.print("Digite o valor de c: ");
        double c = s.nextDouble();

        String resultado = calcularEquacao2Grau(a, b, c);
        System.out.println(resultado);
    }

    /**
     * equaÃ§Ã£o 2Âº grau = aÂ²x + bx + c
     * 
     * @param a valor de a
     * @param b valor de b
     * @param c valor de c
     * @return o resultado de xÂ¹ e xÂ² da equaÃ§Ã£o com as devidas validaÃ§Ãµes
     */
    public static String calcularEquacao2Grau(double a, double b, double c) {
        double delta = Math.pow(b, 2) + (-4 * a * c);

        if (delta < 0)
            return "NÃ£o possui soluÃ§Ã£o real";

        if (delta == 0) {
            double x = -b / (2 * a);
            return "xÂ¹ e xÂ² = " + x;
        } else {
            double x1 = (-b + Math.sqrt(delta)) / (2 * a);
            double x2 = (-b - Math.sqrt(delta)) / (2 * a);

            return "xÂ¹ = " + x1 + ", xÂ² = " + x2;
        }
    }

    /**
     * 
     * @param n nÃºmero da questÃ£o
     */
    public static void escreverQuestao(int n) {
        System.out.println("QuestÃ£o " + n);
    }

    /**
     * 
     * @param n numero de repetiÃ§Ã£o do caractere '='
     */
    public static void separar(int n) {
        System.out.println();
        System.out.println(new String(new char[n]).replace("\0", "="));
    }
}