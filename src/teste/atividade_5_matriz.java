package teste;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class atividade_5_matriz {
    private static Scanner sc = new Scanner(System.in);
    private static Random random = new Random();

  
    public static void main(String[] args) {
        questao01();
        System.out.println();
        questao02();
        System.out.println();
        questao03();
        System.out.println();
        questao04();
        System.out.println();
        questao05();
        System.out.println();
        questao06();
        System.out.println();
        sc.close();
    }

    public static void questao01() {
        double matriz[][] = new double[3][4];
        System.out.println("Preencendo Matriz 3x4: ");

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.print("Digite o valor posicao [" + i + ", " + j + "]: ");
                // matriz[i][j] = sc.nextDouble();
                matriz[i][j] = random.nextDouble();
                System.out.println(matriz[i][j]);
            }
        }

        System.out.println("=== MATRIZ COMPLETA");
        // Completa
        for (int i = 0; i < matriz.length; i++) {
            System.out.println();
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.printf("%10.1f ", matriz[i][j]);
            }
            System.out.println();
        }
        System.out.println("=== CANTO SUPERIOR ESQUERDO");
        // Canto Superior Esquerdo
        for (int i = 0; i < matriz.length; i++) {
            System.out.println();
            for (int j = 0; j < matriz[0].length - (i + 1); j++) {
                System.out.printf("%10.1f ", matriz[i][j]);
            }
            System.out.println();
        }
        System.out.println("=== CANTO INFERIOR DIREITO");
        // Canto Inferior Direito
        for (int i = 0; i < matriz.length; i++) {
            System.out.println();
            for (int j = matriz[0].length - (i + 1); j <= matriz[0].length - 1; j++) {
                System.out.printf("%10.1f ", matriz[i][j]);
            }
            System.out.println();
        }
    }

    public static void questao02() {
        double matriz[][] = new double[5][5];
        double matrizCubo[][] = new double[5][5];
        System.out.println("Preencendo Matriz 5x5: ");

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.print("Digite o valor posicao [" + i + ", " + j + "]: ");
                // matriz[i][j] = sc.nextDouble();
                matriz[i][j] = random.nextInt(10);
                matrizCubo[i][j] = Math.pow(matriz[i][j], 3);

                System.out.println(matriz[i][j]);
            }
        }

        System.out.println("=== MATRIZ ORIGINAL");
        // Matriz Original
        for (int i = 0; i < matriz.length; i++) {
            System.out.println();
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.printf("%10.1f ", matriz[i][j]);
            }
            System.out.println();
        }

        System.out.println("=== MATRIZ CUBO");
        // Matriz Cubo
        for (int i = 0; i < matrizCubo.length; i++) {
            System.out.println();
            for (int j = 0; j < matrizCubo[0].length; j++) {
                System.out.printf("%10.1f ", matrizCubo[i][j]);
            }
            System.out.println();
        }
    }

    public static void questao03() {
        double matriz[][] = new double[3][3];
        System.out.println("Preencendo Matriz 3x3: ");

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.print("Digite o valor posicao [" + i + ", " + j + "]: ");
                // matriz[i][j] = sc.nextDouble();
                matriz[i][j] = random.nextInt(10);

                System.out.println(matriz[i][j]);
            }
        }

        System.out.println("=== SOMA DIAGONAL PRINCIPAL");
        // Diagonal Principal
        System.out.println("Valores => ");
        double soma = 0;
        for (int i = 0; i < matriz.length; i++) {
            System.out.print(matriz[i][i] + " ");
            soma += matriz[i][i];
        }

        System.out.printf("\nTotal => %.2f\n", soma);
    }

    public static void questao04() {
        System.out.println("Criando Matriz Indentidade de NxN: ");
        System.out.print("Digite o valor de N: ");
        // int l = sc.nextInt();

        int l = random.nextInt(10);
        System.out.println(l);

        int matriz[][] = new int[l][l];
        // Diagonal Principal
        for (int i = 0; i < matriz.length; i++) {
            matriz[i][i] = 1;
        }

        System.out.println("=== MATRIZ INDENTIDADE " + l + "x" + l);
        // Matriz Original
        for (int i = 0; i < matriz.length; i++) {
            System.out.println();
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.printf("%5d ", matriz[i][j]);
            }
            System.out.println();
        }
    }

    public static void questao05() {
        System.out.println("Criando Matriz NxN: ");
        System.out.print("Digite o valor de N: ");
        // int l = sc.nextInt();

        int l = random.nextInt(4);
        System.out.println(l);

        int matriz[][] = new int[l][l];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.print("Digite o valor posicao [" + i + ", " + j + "]: ");
                // matriz[i][j] = sc.nextInt();
                matriz[i][j] = random.nextInt(10);
                System.out.println(matriz[i][j]);
            }
        }

        // Matriz Original || Simetrica
        for (int i = 0; i < matriz.length; i++) {
            System.out.println();
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.printf("%5d ", matriz[i][j]);
            }
            System.out.print("\t| |");
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.printf("%5d ", matriz[j][i]);
            }
            System.out.println();
        }

        boolean ehSimetrica = true;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                if (matriz[i][j] != matriz[j][i]) {
                    ehSimetrica = false;
                    break;
                }
            }

            if (!ehSimetrica)
                break;
        }

        System.out.println(ehSimetrica ? "É uma matriz simétrica" : "Não é uma matriz simétrica");
    }

    private static void questao06() {
        System.out.print("Quantos alunos tem na turma: ");
        // int numAlunos = sc.nextInt();

        int numAlunos = random.nextInt(4);
        System.out.println(numAlunos);

        double matrizNotas[][][] = new double[numAlunos][5][4];

        for (int i = 0; i < matrizNotas.length; i++) {
            System.out.println("Aluno " + (i + 1));
            for (int j = 0; j < matrizNotas[0].length; j++) {
                System.out.println("Disciplina: " + (j + 1));
                for (int k = 0; k < matrizNotas[0][0].length; k++) {
                    // matrizNotas[i][j][k] = sc.nextDouble();
                    matrizNotas[i][j][k] = random.nextInt(11);
                    System.out.print("Digite a " + (k + 1) + "º nota: ");
                    System.out.println(matrizNotas[i][j][k]);
                }
            }
        }

        System.out.println("\n=========");
        double maiorNotaTodos = Double.MIN_VALUE;
        String alunoDiciplina = "";
        for (int i = 0; i < matrizNotas.length; i++) {
            System.out.println("Aluno " + (i + 1));
            System.out.println("=========");
            double maiorNotaGeral = Double.MIN_VALUE;
            String disciplina = "";
            for (int j = 0; j < matrizNotas[0].length; j++) {
                System.out.println("Disciplina: " + (j + 1));
                System.out.println("Notas: " + Arrays.toString(matrizNotas[i][j]));
                double maior = Double.MIN_VALUE;
                for (int k = 0; k < matrizNotas[0][0].length; k++) {
                    maior = matrizNotas[i][j][k] > maior ? matrizNotas[i][j][k] : maior;
                }
                System.out.println("Maior Nota: " + maior);
                System.out.println("");
                if (maiorNotaGeral < maior) {
                    maiorNotaGeral = maior;
                    disciplina = "Disciplina " + (j + 1);
                }
            }

            if (maiorNotaTodos < maiorNotaGeral) {
                maiorNotaTodos = maiorNotaGeral;
                alunoDiciplina = disciplina + " - Aluno " + (i + 1);
            }

            System.out.println("Maior nota em todas as disciplinas: " + maiorNotaGeral + " - " + disciplina);
            System.out.println("=========");
        }

        System.out.println("Maior nota entre todos os alunos: " + maiorNotaTodos + " - " + alunoDiciplina);
        System.out.println("=========");

        sc.close();
    }
}