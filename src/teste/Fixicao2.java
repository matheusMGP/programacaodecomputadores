package teste;

import java.util.Scanner;

/**
 * @category Fixicao
 * @author Pedro Lucas - 20191112387
 */
public class Fixicao2 {
    public static Scanner s = new Scanner(System.in);

    public static void main(String[] args) {
        questao01();

        questao02();

        questao03();
    }

    public static void questao01() {
        separar(25);
        escreverQuestao(1);

        figura01();

        System.out.println();

        figura02();

        System.out.println();

        figura03();
    }

    public static void figura01() {
        int numeroAst = 5, numeroLinhas = 4;

        // Criar fileira de '*'
        String asteristico = repetirString("* ", numeroAst);
        for (int i = 1; i <= numeroLinhas; i++) {
            System.out.println(asteristico);
        }
    }

    public static void figura02() {
        int numeroLinhas = 4;

        String asteristico = "";
        for (int i = numeroLinhas; i > 0; i--) {
            String espaco = repetirString(" ", i - 1);
            asteristico += "* ";
            System.out.println(espaco + asteristico);
        }
    }

    public static void figura03() {
        int numeroAstInicioFim = 4, numeroAstMeio = 9, numeroLinhas = 4;
        int numeroEspacosInicioFim = (numeroAstMeio - numeroAstInicioFim) / 2;
        int numeroEspacosMeio = (numeroAstInicioFim - numeroAstMeio) / 2;

        String espacosInicioFim = repetirString(" ", numeroEspacosInicioFim > 0 ? numeroEspacosInicioFim : 0);
        String espacosMeio = repetirString(" ", numeroEspacosMeio > 0 ? numeroEspacosMeio : 0);

        String asteristicoInicioFim = repetirString("*", numeroAstInicioFim);
        String asteristicoMeio = repetirString("*", numeroAstMeio);

        for (int i = numeroLinhas; i > 0; i--) {
            if (i == numeroLinhas || i == 1) {
                System.out.println(espacosInicioFim + asteristicoInicioFim);
            } else {
                System.out.println(espacosMeio + asteristicoMeio);
            }
        }
    }

    /**
     * 
     * @param value String que irá se repetir @param qtd vezes
     * @param qtd   número de vezes que se deve repitir o @param value
     * @return String que contém o @param value repitido @param qtd
     */
    public static String repetirString(String value, int qtd) {
        return new String(new char[qtd]).replace("\0", value);
    }

    public static void questao02() {
        separar(25);
        escreverQuestao(2);

        System.out.print("Digite um valor inteiro: ");
        int numero = s.nextInt();

        System.out.println();
        System.out.println(numero < 0 ? "ERROR - NÚMERO INVÁLIDO" : "NÚMERO VÁLIDO");

    }

    public static void questao03() {
        separar(25);
        escreverQuestao(3);

        System.out.print("Digite o 1º valor: ");
        double n1 = s.nextDouble();

        System.out.print("Digite o 2º valor: ");
        double n2 = s.nextDouble();

        System.out.print("Digite o 3º valor: ");
        double n3 = s.nextDouble();

        double soma = n1 + n2 + n3;

        System.out.println();
        System.out.println(soma > 100 ? "A soma é " + soma : "Infelizmente você não alcançou o valor minimo.");

    }

    /**
     * 
     * @param n número da questão
     */
    public static void escreverQuestao(int n) {
        System.out.println("Questão " + n);
        System.out.println();
    }

    /**
     * 
     * @param n numero de repetição do caractere '='
     */
    public static void separar(int n) {
        System.out.println();
        System.out.println(new String(new char[n]).replace("\0", "="));
    }
}