package teste;

import java.util.Scanner;

/**
 * Fixicao
 */
public class Fixicao {
    private static Scanner sc = new Scanner(System.in);;

    public static void main(final String[] args) {
        separar(25);
        questao01();

        separar(25);
        questao02();
    }

    private static void questao01() {
        escreverQuestao(1);
        System.out.print("Qual é o seu nome? ");
        final String nome = sc.nextLine();

        System.out.println();
        System.out.print("Olá " + nome);
        System.out.println(", seja bem vindo :)");
    }

    private static void questao02() {
        escreverQuestao(2);
        System.out.print("Digite o 1º valor: ");
        double valor1 = sc.nextDouble();

        System.out.print("Digite o 2º valor: ");
        double valor2 = sc.nextDouble();

        System.out.print("Digite o 3º valor: ");
        double valor3 = sc.nextDouble();

        double soma = valor1 + valor2 + valor3;

        System.out.println();
        System.out.println("A soma dos três valores é: " + soma);
    }

    private static void escreverQuestao(int n) {
        System.out.println("Questão " + n);
    }

    private static void separar(int n) {
        System.out.println();
        System.out.println(new String(new char[n]).replace("\0", "="));
    }
}